
kubectl  apply -f https://gitlab.com/intrepid202/spring-boot-jib-gks/-/raw/master/cluster-admin.yml

kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')

export SECRET=kubectl get secrets | tail -n 1  | awk '{print $1}’)

kubectl get secret $SECRET -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
